# %% Important imports
import itkdb

from matplotlib import cm
import matplotlib.pyplot as plt

import numpy as np
import pandas as pd

import metrodb

import sys
import argparse
# %% Prepare configuration
if 'ipykernel' in sys.modules: # running in a notebook
    # %load_ext autoreload
    # %autoreload 2
    #module='20USBML1234774'
    localid=True
    module='SCIPP-PPB_LS-029'
else:
    parser=argparse.ArgumentParser(description="Plot module/sensor bow results.")
    parser.add_argument('--localid',action='store_true',help='serial number is the stupid local name')
    parser.add_argument('module', help='module serial number')
    args=parser.parse_args()

    localid=args.localid
    module=args.module

# %% Prepare the datbase client
c = itkdb.Client(save_auth='.auth')
c.user.authenticate()

# %%
cmap = cm.get_cmap('viridis', 12)

# %% Get the module information
if localid:
    data={'filterMap': {
        'project':'S',
        'componentType':'MODULE',
        'propertyFilter': [
            {'code':'LOCALNAME', 'operator':'=', 'value':module}
        ]}}
    module_data=c.get('listComponentsByProperty', json=data)
    module_data=next(module_data)
    module=module_data['serialNumber']

data={'component': module}
module_data=c.get('getComponent',json=data)

print(f'Processing module {module}')

# %% Get the requested component
data={'filterMap':{'serialNumber': module, 'testType':'MODULE_BOW'}}
result=c.get('listTestRunsByComponent', json=data)
testRuns=list(map(lambda testRun: testRun['id'], result))
# %%
data={'testRun': testRuns}
result=c.get('getTestRunBulk', json=data)

# %% Plot the metrology data
for testRun in result:
    bow=metrodb.get_result(testRun, 'BOW')
    if len(testRun['attachments'])==0:
        continue; 

    response = c.get("uu-app-binarystore/getBinaryData",
        json={"code": testRun['attachments'][0]['code']})

    df=pd.read_csv(response.filename, sep='\s+', skiprows=13, names=['Location','X','Y','Z'])

    fig=plt.figure()
    ax=fig.add_subplot(projection='3d')

    x=np.vstack([df.X,df.Y,np.ones(len(df.index))]).T
    surf,_,_,_=np.linalg.lstsq(x,df.Z)
    plane=np.dot(x,surf)
    df['Z_BOW']=df.Z-plane

    # Quick bow calculation to check
    zmin=df.Z_BOW.min()
    zmax=df.Z_BOW.max()
    mybow=zmax-zmin
    if abs(zmax)<abs(zmin):
        mybow*=-1
    print(f'MYBOW = {mybow}')

    surf = ax.plot_trisurf(df.X,df.Y,df.Z_BOW*1000, linewidth=0.5, cmap=cmap, edgecolors='black')
    ax.scatter(df.X,df.Y,df.Z_BOW*1000)

    ax.set_xlabel('X [mm]')
    ax.set_ylabel('Y [mm]')
    ax.set_zlabel('Z [$\mu$m]')

    if df.X.min()<0:
        ax.set_xlim(-50,50)
    else:
        ax.set_xlim(0,100)
    if df.Y.min()<0:
        ax.set_ylim(-50,50)
    else:
        ax.set_ylim(0,100)
    ax.set_zlim(-100,100)

    fig.suptitle(f'{module}, bow = {bow:0.0f} um')

    fig.tight_layout()

    fig.savefig(f'{module}_module.png')
    fig.show()

# %% Find the sensor
sensor=next(filter(lambda child: child['componentType']['code']=='SENSOR' and child['type']['code'].startswith('ATLAS18'),module_data['children']))

# %% Get the requested component
data={'filterMap':{'serialNumber': sensor['component']['serialNumber'], 'testType':'ATLAS18_SHAPE_METROLOGY_V1'}}
result=c.get('listTestRunsByComponent', json=data)
testRuns=list(map(lambda testRun: testRun['id'], result))
# %%
data={'testRun': testRuns}
result=c.get('getTestRunBulk', json=data)

# %% Plot the metrology data
for testRun in result:
    # Extract the values    
    x=np.array(metrodb.get_result(testRun, 'X'))
    y=np.array(metrodb.get_result(testRun, 'Y'))
    z=np.array(metrodb.get_result(testRun, 'Z_BOW'))

    # Plot the data
    fig=plt.figure()
    ax=fig.add_subplot(projection='3d')

    surf = ax.plot_trisurf(x,y,z*1000, linewidth=0.5, cmap=cmap, edgecolors='black')
    ax.scatter(x,y,z*1000)

    ax.set_xlabel('X [mm]')
    ax.set_ylabel('Y [mm]')
    ax.set_zlabel('Z [$\mu$m]')

    if x.min()<0:
        ax.set_xlim(-50,50)
    else:
        ax.set_xlim(0,100)
    if y.min()<0:
        ax.set_ylim(-50,50)
    else:
        ax.set_ylim(0,100)
    ax.set_zlim(-100,100)

    fig.suptitle(f'{module}')

    fig.tight_layout()

    fig.savefig(f'{module}_sensor.png')
    fig.show()

# %%
