# Ploting of metrology results from the ITk PDB.

## Installation
The `setup.py` script contains all of the necessary dependencies.
```shell
pipenv install .
```

## Authentication
One needs to authenaticate with the ITk PDB before running all of the scripts. Run the following at the start of every session or whenever you get an error indicating that your token has expired.

```shell
pipenv run itkdb authenticate
```

## Included Scripts
Make sure the authenticate with the ITk PDB before runnning any of the following scripts.

### Module/Sensor Surface Bow
A 3D surface plot of the module (after assembly) and sensor (before assembly) metrology is drawn. The sensor is determined from provided module ID. The tilt correction is recomputed and applied before plotting.

```
usage: modulemetrology.py [-h] [--localid] module

Plot module/sensor bow results.

positional arguments:
  module      module serial number

options:
  -h, --help  show this help message and exit
  --localid   serial number is the stupid local name
```