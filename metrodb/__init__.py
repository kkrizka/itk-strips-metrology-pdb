def get_result(json, code):
    return next(filter(lambda p: p['code']==code, json['results']))['value']
